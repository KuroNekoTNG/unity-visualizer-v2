﻿using System;
using System.IO;

namespace Libraries {
	/// <summary>
	/// Library acts as the main class for the extension.
	/// This class is <see langword="abstract"/>, it is advised to either extend this directly, or extend on of the higher ups like <see cref="AudioCodec"/>.
	/// </summary>
	public abstract class Library {

		/// <summary>
		/// Holds the path of the extension. It should be the parent folder of the assembly's folder.
		/// </summary>
		protected DirectoryInfo extensionHome;

		/// <summary>
		/// Used to create the library and set things up.
		/// </summary>
		/// <param name="home">The path to the extensions resourses.</param>
		protected Library( string home ) {
			if ( string.IsNullOrWhiteSpace( home ) || !Directory.Exists( home ) ) { // Checks to see if path is null, white space, empty, or if the path it points to does not exist.
				throw new ArgumentException( "The argument was either null, or the path does not exist.", "path" ); // Throws a new argument exception.
			}

			extensionHome = new DirectoryInfo( home );
		}

		/// <summary>
		/// Used to load the dependencies of the assembly.
		/// </summary>
		/// <returns>A string array of assemblies.</returns>
		public string[] Dependencies() {
			string dependsPath = Path.Combine( extensionHome.FullName, "Dependency" ); // Creates a new path.

			if ( Directory.Exists( dependsPath ) ) { // Checks to see if it exists.
				return Directory.GetFiles( dependsPath, "*.dll", SearchOption.AllDirectories ); // Grabs all of the assemblies.
			} else {
				return null; // Returns nothing.
			}
		}

	}
}