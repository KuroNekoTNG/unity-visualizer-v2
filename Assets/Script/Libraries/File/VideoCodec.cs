﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Libraries.File {
	/// <summary>
	/// Codec for video files.
	/// Since video files, on average, contains both an audio stream and a video stream, this class extends <see cref="AudioCodec"/>.
	/// It is planned to add a video element to the visualizer. For now, it is best to use <see cref="AudioCodec"/>.
	/// </summary>
	public abstract class VideoCodec : AudioCodec {

		/// <summary>
		/// This constructor builds up the object.
		/// See also: <seealso cref="AudioCodec"/>, <seealso cref="Codec"/>, and <seealso cref="Library"/>
		/// </summary>
		/// <param name="home"></param>
		public VideoCodec( string home ) : base( home ) {

		}

		/// <summary>
		/// Does the same as <see cref="VideoCodec(string)"/>.
		/// </summary>
		/// <param name="home">The extensions home folder.</param>
		/// <param name="exts">Supported extensions.</param>
		protected VideoCodec( string home, params string[] exts ) : base( home, exts ) {
		}
	}
}
