﻿using System.IO;

namespace Libraries.File {

	/// <summary>
	/// Used to convert the extensions it supports to a float array.
	/// This class is <see langword="abstract"/>, it also extends <see cref="Codec"/>.
	/// </summary>
	public abstract class AudioCodec : Codec {
		/// <summary>
		/// This constructor is used to set up the Codec object.
		/// </summary>
		/// <param name="home">The path of the extensions resources.</param>
		public AudioCodec( string home ) : base( home ) {

		}

		/// <summary>
		/// This constructor set's up this Codec object, however, this is to be used internally.
		/// </summary>
		/// <param name="home">The path of the extensions resources.</param>
		/// <param name="exts">An array of extensions supported by this codec.</param>
		public AudioCodec( string home, params string[] exts ) : base( home, exts ) {

		}

		/// <summary>
		/// Grabs the samples from the audio file passed to it.
		/// </summary>
		/// <param name="audioFile">The full name of the audio file that is needed to load.</param>
		/// <returns>A float array holding all of the samples.</returns>
		public virtual float[] AudioSamples( string audioFile ) {
			if ( System.IO.File.Exists( audioFile ) ) {
				return AudioSamples( new FileStream( audioFile, FileMode.Open, FileAccess.Read, FileShare.None, 4096, true ) );
			} else {
				throw new FileNotFoundException( "Audio file does not exist.", audioFile );
			}
		}

		/// <summary>
		/// Grabs the samples from the audio file passed to it.
		/// See also: <seealso cref="AudioSamples(string)"/>
		/// </summary>
		/// <param name="stream">The stream that will be used to load the audio clip.</param>
		/// <returns>A float array holding all of the samples.</returns>
		public abstract float[] AudioSamples( Stream stream );

		/// <summary>
		/// Retrives the audio clips title.
		/// </summary>
		/// <returns>The title of the clip.</returns>
		public abstract string AudioTitle();

		/// <summary>
		/// Retrives the person, or people, or group who created the audio clip.
		/// </summary>
		/// <returns>The person, or people, or group who created the clip.</returns>
		public abstract string AudioArtist();

		/// <summary>
		/// Retrives the album that the song is in.
		/// </summary>
		/// <returns>The albums name.</returns>
		public abstract string AudioAlbum();
	}
}
