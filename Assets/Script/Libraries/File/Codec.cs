﻿using System.Collections.Generic;

namespace Libraries.File {
	/// <summary>
	/// A general codec class used to load audio or video files.
	/// This class is <see langword="abstract"/> and extends from <see cref="Library"/>.
	/// </summary>
	public abstract class Codec : Library {

		/// <summary>
		/// Used to retrive a list read only list of files it can play.
		/// </summary>
		public IReadOnlyList<string> Plays {
			get;
		}

		/// <summary>
		/// The default Codec constructor.
		/// See also: <seealso cref="Library(string)"/>
		/// </summary>
		/// <param name="home"></param>
		public Codec( string home ) : base( home ) {
			Plays = new List<string>(); // Creates an empty Plays.
		}

		/// <summary>
		/// Generates a new <see cref="IReadOnlyCollection{string}"/>.
		/// See also: <seealso cref="Codec(string)"/>, <seealso cref="Library(string)"/>
		/// </summary>
		/// <param name="home"></param>
		/// <param name="exts"></param>
		protected Codec( string home, params string[] exts ) : base( home ) {
			List<string> playExts = new List<string>(); // Creates a new list to store the values.

			foreach ( string ext in exts ) { // Loops through the all the extensions passed to it.
				if ( ext.StartsWith( "." ) ) { // Checks to see if they start with the extension start marker.
					playExts.Add( ext ); // Adds it to the list of extensions.
				}
			}

			Plays = playExts; // Assigns the Plays to the generated list.
		}
	}
}