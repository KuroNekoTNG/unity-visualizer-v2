﻿using System;
using System.Collections.Generic;
using System.IO;

using Newtonsoft.Json;

using UnityEngine;

namespace Visualizer.Core.Handlers {

	/// <summary>
	/// Language Manager is a singleton that ensures a stable way of gathering language files and loading them into the program.
	/// It ensures that no memory is wasted and only loads the one that is currently selected.
	/// It uses a delegate to send out a signal that the language has changed. This should be enforced as it will make sure that every asset uses that language.
	/// </summary>
	public class LanguageManager {

		#region Static Properties and Fields
		/// <summary>
		/// Used to retrive the language manager to pull data from.
		/// </summary>
		public static LanguageManager Singleton {
			get {
				if ( singleton == null ) { // Checks to see if it has been created or not.
					singleton = new LanguageManager(); // Creates a new language manager.
				}

				return singleton; // Returns the language manager.
			}
		}

		/// <summary>
		/// The path where the language files are located.
		/// </summary>
		public static readonly string LANGUAGE_PATH = Path.Combine( Application.streamingAssetsPath, "Language" );

		private static LanguageManager singleton; // Holds the reference to the language manager.
		#endregion

		/// <summary>
		/// The language that is currently selected.
		/// </summary>
		public string CurrentLanguage {
			get {
				return currentLanguage; // Returns the current language.
			}

			set {
				currentLanguage = value; // Set's the new language to the one passed to it.
				LoadDictionary(); // Loads the new language.
				Changed(); // Notifies all users of delegate Changed to change their text.
			}
		}

		/// <summary>
		/// An array of languages avialable.
		/// </summary>
		public string[] Languages {
			get;
			private set;
		}

		/// <summary>
		/// Used to retrive a string paired with the key.
		/// It is safe to use this.
		/// </summary>
		/// <param name="key">The string to retrive the paired value.</param>
		/// <returns>The paired value if the key exists, otherwise returns an error string.</returns>
		public string this[string key] {
			get {
				string value; // Cannot use inlining variables since Unity uses C# 6. UwU

				if ( TryGetValue( key, out value ) ) { // Tries to get the value from the dictionary.
					return value; // Returns the value if successful.
				} else {
					Debug.Log( $"Key, '{key}' was not found." );
					return errorMessages[UnityEngine.Random.Range( 0, errorMessages.Length )]; // Returns a random error message.
				}
			}
		}

		/// <summary>
		/// Used to spread the message that the languge has changed.
		/// </summary>
		public delegate void NotifyLanguageChange();

		/// <summary>
		/// This is used to notify subscibed methods that the current language has changed.
		/// </summary>
		public NotifyLanguageChange Changed;

		private string currentLanguage; // Holds the current language that is in use.
		private readonly string[] errorMessages = { "ERROR_FEMBOI_FOX_NOT_FOUND", "Roses are red\nViolets are blue\nThis program was made by a furry\nUwU I errored." };

		private Dictionary<string, string> langDictionary; // Holds the key-value pairs of that language.
		private SettingsManager settingsManager; // Holds the reference to the SettingsManager as to stop a bunch of SettingsManager.Singleton calls.
		private DirectoryInfo langDir;

		private LanguageManager() {
			settingsManager = SettingsManager.Singleton; // Get's the reference to the singleton.
			langDictionary = new Dictionary<string, string>(); // Creates a new langDictionary.
			langDir = new DirectoryInfo( LANGUAGE_PATH ); // Creates a new DirectoryInfo to enumerate over the files.

			if ( !settingsManager.ConstainsKey( "lang" ) ) { // Checks if settings contains this manager's value.
				settingsManager.Add( "lang", "English" ); // Adds the key and a default value to the settings manager.
				currentLanguage = "English"; // Set's current language to the default value.
			} else {
				try { // Tries to grab the default value.
					currentLanguage = settingsManager.GetValue( "lang" ) as string; // Grabs the default value and casts it to a string.
				} catch {
					currentLanguage = "English"; // If an exception is thrown, default to english.
				}
			}

			GetLanguages();
			LoadDictionary(); // Loads the dictionary.
		}

		private void GetLanguages() {
			List<string> languages = new List<string>(); // Creates an empty list.

			foreach ( FileInfo file in langDir.EnumerateFiles( "*.json", SearchOption.TopDirectoryOnly ) ) { // Loops through every instance of the .json language files.
#if UNITY_EDITOR
				Debug.Log( $"Found lang file: {file.FullName}" ); // Writes this to the console.
#endif

				languages.Add( file.Name.Substring( 0, file.Name.LastIndexOf( '.' ) ) ); // Adds the name of the language file into the actual array of substrings.
			}

			Languages = languages.ToArray(); // Grabs the array from the list of languages and assigns it to the Languages property.
		}

		private void LoadDictionary() { // Loads the dictionary with the new language set.
			string path = Path.Combine( LANGUAGE_PATH, $"{CurrentLanguage}.json" ); // Generates the path that the language pack should be at.

			if ( File.Exists( path ) ) { // Checks to see if the pack exists.
				langDictionary.Clear(); // Clears the dictionary before populating it.

				langDictionary = JsonConvert.DeserializeObject<Dictionary<string, string>>( File.ReadAllText( path ), SettingsManager.SERIALIZER_SETTINGS ); // See Newtonsoft.Json doc's.
			} else {
				throw new Exception( $"Unable to find language pack file: '{path}'" ); // Warns that it was unsuccessful.
			}
		}

		/// <summary>
		/// Used to get the value paired with the key for the language.
		/// Exceptions:
		/// <list type="bullet">
		/// <item>
		/// <see cref="KeyNotFoundException"/>
		/// </item>
		/// </list>
		/// </summary>
		/// <param name="key">Used to retrive the value.</param>
		/// <returns>The string paired with the key.</returns>
		public string GetValue( string key ) => langDictionary[key];

		/// <summary>
		/// Used to get the value paired with the key for the language.
		/// See Also: <seealso cref="GetValue(string)"/>
		/// </summary>
		/// <param name="key">Used to retrive the value.</param>
		/// <param name="value">The value paired with the key.</param>
		/// <returns>True if the key exists and the value was successfully retrived, otherwise false.</returns>
		public bool TryGetValue( string key, out string value ) {
			try {
				value = GetValue( key ); // Get's the value using the raw method.
				return true; // Returns true to indicate success.
			} catch {
				value = null; // Set's value to null.
				return false; // Returns false to indicate failure.
			}
		}
	}
}
