﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

using Newtonsoft.Json;

using UnityEngine;

namespace Visualizer.Core.Handlers {

	/// <summary>
	/// Used to store settings relating to program.
	/// </summary>
	public class SettingsManager {

		#region Static Property and Fields
		/// <summary>
		/// Used to retrive a reference to the manager.
		/// </summary>
		public static SettingsManager Singleton {
			get {
				if ( singleton == null ) { // Checks if the singleton is null.
					singleton = new SettingsManager(); // Creates a new singleton.
				}

				return singleton; // Returns the singleton.
			}
		}

		/// <summary>
		/// The default for writing to a JSON file.
		/// For infromation on this, see: <see cref="JsonSerializerSettings"/>
		/// Note: This program does use Newtonsoft.Json since Unity's JSON converter does not fully serialize objects.
		/// </summary>
		public static readonly JsonSerializerSettings SERIALIZER_SETTINGS = new JsonSerializerSettings() {
			DateFormatHandling = DateFormatHandling.IsoDateFormat,
			ConstructorHandling = ConstructorHandling.AllowNonPublicDefaultConstructor,
			DateParseHandling = DateParseHandling.DateTime,
			DateTimeZoneHandling = DateTimeZoneHandling.Utc,
			FloatFormatHandling = FloatFormatHandling.Symbol,
			DefaultValueHandling = DefaultValueHandling.Populate | DefaultValueHandling.Include,
			FloatParseHandling = FloatParseHandling.Double,
			Formatting = Formatting.Indented,
			MetadataPropertyHandling = MetadataPropertyHandling.ReadAhead,
			MissingMemberHandling = MissingMemberHandling.Ignore,
			NullValueHandling = NullValueHandling.Include,
			ObjectCreationHandling = ObjectCreationHandling.Auto,
			ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
			PreserveReferencesHandling = PreserveReferencesHandling.None,
			StringEscapeHandling = StringEscapeHandling.Default,
			TypeNameHandling = TypeNameHandling.None,
			TypeNameAssemblyFormatHandling = TypeNameAssemblyFormatHandling.Simple
		};

		private static SettingsManager singleton; // Stores the reference to the singleton. 
		#endregion

		private string settingsFile; // Holds the reference to the settings file.
		private int before = 0;

		private Dictionary<string, object> settings; // Holds all the settings that the Visualizer will use.

		/// <summary>
		/// Used to get and set values from the manager.
		/// Note: This does not check to verify that the key exists at all, use at risk.
		/// </summary>
		/// <param name="key">The key to access the value.</param>
		/// <returns>The value that the key refrences.</returns>
		public object this[string key] {
			get {
				return GetValue( key );
			}

			set {
				SetValue( key, value );
			}
		}

		private SettingsManager() { // Prevents others from creating a new instance of the object.
			settingsFile = Path.Combine( Application.persistentDataPath, "settings.json" ); // Creates a path that references the settings.

			settings = new Dictionary<string, object>();

			if ( File.Exists( settingsFile ) ) { // Checks if that file exists.
				try {
					settings = JsonConvert.DeserializeObject<Dictionary<string, object>>( File.ReadAllText( settingsFile ), SERIALIZER_SETTINGS ); // Deserializes the settings file using Newtonsoft.Json.
				} catch ( JsonSerializationException ) {
					Debug.LogWarning( "Failed to deserialize the settings file." ); // Logs that it failed to deserialize the settings json file.
				} catch ( Exception e ) {
					Debug.LogException( e ); // Something happened, and it will log an exception.
				}
			} else {
				File.Create( settingsFile ).Close(); // Creates a new settings file and closes the returned stream.
			}

			before = settings.GetHashCode(); // Grabs the hash code for comparing later.

			Application.quitting += SaveSettings; // Used to ensure that the settings are saved upon quitting.
		}

		private void SaveSettings() { // Used to save settings upon closing.
			string newValues = "";
#if UNITY_EDITOR // THIS IS FOR LATER!
			StringBuilder builder = new StringBuilder();
#endif

			newValues = JsonConvert.SerializeObject( settings, SERIALIZER_SETTINGS ); // Serializes the settings dictionary.

			File.WriteAllText( settingsFile, newValues ); // Writes the new string to the settings file.

#if UNITY_EDITOR // THIS IS FOR DEBUG PURPOSES. THIS IS TO CHECK FOR ANY ERRORS IN THE SETTINGS FILE.
			foreach ( KeyValuePair<string, object> pair in settings ) {
				builder.AppendLine( $"Key: {pair.Key}\nValue: {pair.Value}" );
			}

			Debug.Log( $"Finished SaveSettings method.\n{builder.ToString()}" );
#endif
		}

		/// <summary>
		/// Returns the value that the key refrences.
		/// Note: This does not check to verify that the key exists, use at risk.
		/// See also: <seealso cref="this[string]"/>
		/// </summary>
		/// <param name="key">Used to get the value.</param>
		/// <returns>The value that pairs with the key.</returns>
		public object GetValue( string key ) => settings[key];

		/// <summary>
		/// Tries to retrive the value that is suppose to be stored with the key.
		/// </summary>
		/// <param name="key">The key that is paired with the value.</param>
		/// <param name="value">The value that is paired with the key.</param>
		/// <returns>True if the key exists and a value was outputed, otherwise false.</returns>
		public bool TryGetValue( string key, out object value ) {
			if ( settings.ContainsKey( key ) ) { // Checks to see if the dictionary contains the key.
				value = GetValue( key ); // Set's the out variable to the value paired with the key.
				return true; // Returns true to indicate success.
			} else {
				value = null; // Set's the out value to null.
				return false; // Returns false to indicate failure.
			}
		}

		/// <summary>
		/// Sets a value to be paired with a key.
		/// See also: <seealso cref="this[string]"/>
		/// </summary>
		/// <param name="key">That will be paired with a value.</param>
		/// <param name="value">The object that will be linked to the key.</param>
		public void SetValue( string key, object value ) => settings[key] = value;

		/// <summary>
		/// Used to ensure that the setting was saved.
		/// </summary>
		/// <param name="key">The object to retrive the value.</param>
		/// <param name="value">The object paired to the key.</param>
		/// <returns>True if the key existed, otherwise false.</returns>
		public bool TrySetValue( string key, object value ) {
			if ( settings.ContainsKey( key ) ) { // Checks to see if key exists.
				SetValue( key, value ); // Set's the key's paired value to the value.
				return true; // Returns true to indicate success.
			} else {
				return false; // Returns false to indicate failure.
			}
		}

		/// <summary>
		/// Adds a key, value pair to the settings.
		/// </summary>
		/// <param name="key">The key that will be used to retrive the value again.</param>
		/// <param name="value">The value to store.</param>
		public void Add( string key, object value ) => settings.Add( key, value );

		/// <summary>
		/// Used to check if a key exists already.
		/// </summary>
		/// <param name="key">The key to see if it exists.</param>
		/// <returns>True if the key exists, otherwise false.</returns>
		public bool ConstainsKey( string key ) => settings.ContainsKey( key );

		/// <summary>
		/// Used to check if settings already contains a value.
		/// </summary>
		/// <param name="value">The value.</param>
		/// <returns>True if the value exists, otherwise false.</returns>
		public bool ConstainsValue( object value ) => settings.ContainsValue( value );
	}
}