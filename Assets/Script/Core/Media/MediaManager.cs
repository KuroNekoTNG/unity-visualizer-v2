﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

using UnityEngine;

namespace Visualizer.Core.Media {
	/// <summary>
	/// Manages all the media extensions.
	/// </summary>
	public class MediaManager {

		#region Static Properties & Fields

		/// <summary>
		/// Used to retrive the MediaManager singleton.
		/// </summary>
		public static MediaManager Singleton {
			get {
				if ( singleton == null ) { // Checks to see if the singleton is null.
					singleton = new MediaManager(); // Creates a new one.
				}

				return singleton; // Returns the singleton.
			}
		}

		private static MediaManager singleton; // Holds the singleton.

		#endregion

		/// <summary>
		/// Holds a reference to the location where it loads extensions from.
		/// </summary>
		public string PersistanceExtensionPlace {
			get;
		}

		/// <summary>
		/// Holds a reference to the location where it loads extensions from.
		/// </summary>
		public string InstallExtensionPlace {
			get;
		}

		
		private AppDomain domain;

		/// <summary>
		/// Private constructor of the singleton.
		/// </summary>
		private MediaManager() {
			domain = AppDomain.CurrentDomain;
			PersistanceExtensionPlace = Path.Combine( Application.persistentDataPath, "Extensions" );
			InstallExtensionPlace = Path.Combine( Directory.GetParent( Application.streamingAssetsPath ).FullName, "Extensions" );

			LoadExtensions();
		}

		/// <summary>
		/// Reloads all the extensions.
		/// </summary>
		public void ReloadExtensions() {

		}

		private void LoadExtensions( string path = "" ) {
			string[] extensions;

			if ( string.IsNullOrWhiteSpace( path ) ) {
				LoadExtensions( PersistanceExtensionPlace );
				LoadExtensions( InstallExtensionPlace );
				return;
			}

			extensions = Directory.GetDirectories( path, "*", SearchOption.TopDirectoryOnly );

			foreach ( string extension in extensions ) {
				if ( IsAudioExtension( extension ) || IsVideoExtension( extension ) ) {
					LoadAudioExtension( extension );
				} else {
					continue;
				}
			}
		}

		private void LoadAudioExtension( string extension ) {
			byte[][] assembyBytes = GetAssemblyBytes( extension );

			foreach ( byte[] assembly in assembyBytes ) {
				domain.Load( assembly );
			}
		}

		private byte[][] GetAssemblyBytes( string extension ) {
			List<byte[]> assemblies = new List<byte[]>();
			List<string> depends = new List<string>( File.ReadAllLines( Path.Combine( "Assemblies", "Requires.list" ) ) );

			RemoveExcessAssemblies( ref depends );

			foreach ( string assembly in depends ) {
				assemblies.Add( File.ReadAllBytes( assembly ) );
			}

			return assemblies.ToArray();
		}

		private void RemoveExcessAssemblies( ref List<string> depends ) {
			foreach ( Assembly ass in domain.GetAssemblies() ) {
				for ( int i = 0; i < depends.Count; ++i ) {
					if ( ass.GetName().Name == depends[i] ) {
						depends.RemoveAt( i );
						break;
					}
				}
			}
		}

		private bool IsVideoExtension( string extension ) {
			string videoFile = Path.Combine( extension, "video" );

			return File.Exists( videoFile );
		}

		private bool IsAudioExtension( string extension ) {
			string audioFile = Path.Combine( extension, "audio" );

			return File.Exists( audioFile );
		}
	}
}