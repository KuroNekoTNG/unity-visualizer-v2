﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

using UnityEngine;

namespace Visualizer.Core.Plugins {
	public static class PluginManager {

		public static string PluginDir {
			get;
		}

		public static DirectoryInfo PluginDirInfo => new DirectoryInfo( PluginDir );

		public static IReadOnlyCollection<Plugin> Plugins => plugins.AsReadOnly();

		private static List<Plugin> plugins;

		static PluginManager() {
			PluginDir = Application.isEditor ?
				null :
				Path.Combine( Application.dataPath, "..", "Plugins" );

			plugins = new List<Plugin>();

			LoadPlugins();
		}

		private static void LoadPlugins() {
			foreach ( var pluginDir in PluginDirInfo.EnumerateDirectories( "*", SearchOption.TopDirectoryOnly ) ) {
				var pluginPack = new PluginPack( pluginDir );

				LoadAsm( pluginPack );
			}
		}

		private static void LoadAsm( PluginPack pluginPack ) {
			foreach ( var asmPath in Directory.EnumerateFiles( pluginPack.AssemblyDir, "*.dll", SearchOption.TopDirectoryOnly ) ) {
				var asm = Assembly.LoadFrom( asmPath );

				FindAndLoadPlugin( asm, pluginPack );
			}
		}

		private static void FindAndLoadPlugin( Assembly asm, PluginPack pluginPack ) {
			var types = asm.GetTypes();

			foreach ( var type in types ) {
				Type baseType;

				do {
					baseType = type.BaseType;
				} while ( baseType.BaseType != null && baseType.BaseType != typeof( Plugin ) );

				if ( baseType is null ) {
					continue;
				}

				var pluginCons = type.GetConstructors().Where( c => c.GetParameters().Length == 1 ).FirstOrDefault();

				var plugin = ( Plugin )pluginCons.Invoke( new[] { ( object )pluginPack } );

				plugins.Add( plugin );
			}
		}
	}
}
