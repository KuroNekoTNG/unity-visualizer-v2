﻿using System.IO;

namespace Visualizer.Core.Plugins {
	public struct PluginPack {

		public string RootDir {
			get;
		}

		public string AssemblyDir {
			get;
		}

		public string LangDir {
			get;
		}

		public string MusicDir {
			get;
		}

		public string TextureDir {
			get;
		}

		public PluginPack( string dir ) {
			RootDir = dir;

			AssemblyDir = Path.Combine( RootDir, "Assembly" );
			LangDir = Path.Combine( RootDir, "Lang" );
			MusicDir = Path.Combine( RootDir, "Music" );
			TextureDir = Path.Combine( RootDir, "Texture" );
		}

		public PluginPack( DirectoryInfo dir ) : this( dir.FullName ) {

		}
	}
}
