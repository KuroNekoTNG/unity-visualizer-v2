﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Visualizer.Core.Plugins {
	public abstract class Plugin {

		protected PluginPack pack;

		public Plugin( PluginPack pack ) {
			this.pack = pack;
		}
	}
}
