﻿using System;

using UnityEngine;

using Visualizer.Core.Handlers;
using Visualizer.Core.Presenter;

namespace Visualizer.Core.Audio {

	[RequireComponent( typeof( AudioSource ), typeof( VisualizerManager ) )]
	public class Bands : MonoBehaviour {

		#region Static Fields

		private static float[] samples = new float[512]; // Stores the samples.
		private static float[] freqBand; // Stores data for each band.
		private static float[] bandBuffer; // Stores the buffered band data. 

		#endregion

		/// <summary>
		/// Set's the speed at which the buffered bands decrease by.
		/// </summary>
		public float DecreaseSpeed {
			get {
				return decreaseSpeed; // Returns the value.
			}

			set {
				if ( value > 1.15f && value < 5f ) { // Checks to see if value is less than 5 and greater than 1.15.
					decreaseSpeed = value; // Assigns decreaseSpeed to the value if true.

					if ( !settings.TrySetValue( "DecreaseRate", value ) ) {
						settings.Add( "DecreaseRate", value );
					}
				} else {
					throw new ArgumentOutOfRangeException( $"[{value}] is out of the proper range. The proper range is 1.15 to 5." ); // Throws this exception since it is out of range.
				}
			}
		}

		/// <summary>
		/// Used to set the FFT Window, which in turns changes how the visualizer looks.
		/// </summary>
		public FFTWindow FFT_Window {
			get {
				return fftWindow; // Returns the fftWindow value.
			}

			set {
				fftWindow = value;

				if ( !settings.TrySetValue( "FFTWindow", value.ToString() ) ) {
					settings.Add( "FFTWindow", value.ToString() );
				}
			}
		}

		[Range( 1.15f, 5f ), SerializeField]
		private float decreaseSpeed = 1.25f; // How fast in general the buffer band should decrease by.
		private float[] bandDecrease; // Stores the data of how fast the bands should shrink.
		private int numberOfBands; // Amount of bands to use.

		[SerializeField]
		private FFTWindow fftWindow = FFTWindow.Hanning;

		private VisualizerManager visualizerManager; // Used to get the number of boxes.
		private AudioSource audioSource; // Used to pull the audio data out of.
		private SettingsManager settings; // Holds the reference to the settings manager.

		// Use this for initialization
		private void Start() {
			settings = SettingsManager.Singleton; // Grabs the reference and stores it for the settings manager.

			GetSettings();

			if ( visualizerManager == null ) { // Checks to see if cirlce center is null.
				visualizerManager = GetComponent<VisualizerManager>(); // Get's the component for circle center.
			}

			if ( audioSource == null ) { // Checks to see if audio source is null.
				audioSource = GetComponent<AudioSource>(); // Get's the component for the audio source.
			}

			numberOfBands = visualizerManager.NumberOfBoxes; // The number of bands is proportional to the number of boxes.
			bandDecrease = new float[numberOfBands]; // Creates a new float array for use later.
			freqBand = new float[numberOfBands]; // Creats a new float array to store the band numbers in.
			bandBuffer = new float[numberOfBands]; // Creates a new float array to store the band buffer.
		}

		private void GetSettings() {
			object tmp; // Used to store a reference to the string that represents the FFTWindow.

			// Tries to get the value from settings and also attempts to parse it.
			if ( settings.TryGetValue( "FFTWindow", out tmp ) ) {
				try {
					fftWindow = ( FFTWindow )Enum.Parse( typeof( FFTWindow ), ( string )tmp );
				} catch ( Exception e ) {
					Debug.LogException( e, this );
					FFT_Window = fftWindow;
				}
			} else {
				FFT_Window = fftWindow;
			}

			if ( settings.TryGetValue( "DecreaseRate", out tmp ) ) {
				try {
					decreaseSpeed = Convert.ToSingle( tmp );
				} catch ( InvalidCastException e ) {
					Debug.LogException( e, this );
					DecreaseSpeed = decreaseSpeed;
				}
			} else {
				DecreaseSpeed = decreaseSpeed;
			}
		}

		// Update is called once per frame
		private void Update() {
			if ( visualizerManager.NumberOfBoxes != numberOfBands ) {
				numberOfBands = visualizerManager.NumberOfBoxes;
				bandDecrease = new float[numberOfBands];
				freqBand = new float[numberOfBands];
				bandBuffer = new float[numberOfBands];
			}

			audioSource.GetSpectrumData( samples, 1, fftWindow );

			GenerateBands();
			GenerateBufferBand();
		}

		/// <summary>
		/// Get's the bands from the index passed to it.
		/// If it recieved any exception, it is automatically caught and logged.
		/// </summary>
		/// <param name="index">The index of the value it is trying to get.</param>
		/// <returns>Returns the float at that index, otherwise it returns float.NaN.</returns>
		public float GetBand( int index ) {
			try {
				return freqBand[index]; // Tries to return the float at the index location.
			} catch ( Exception e ) {
				Debug.LogException( e, this ); // Logs the exception out to the Unity log.
				return float.NaN; // Returns float.NaN since this method has to return a value.
			}
		}

		/// <summary>
		/// Get's the buffered form of the band value.
		/// </summary>
		/// <param name="index">The index to get the float value.</param>
		/// <returns>Returns the value at the location, otherwise it returns float.NaN.</returns>
		public float GetBandBuffer( int index ) {
			try {
				return bandBuffer[index]; // Tries to return the buffered band at that index location.
			} catch ( System.Exception e ) {
				Debug.LogException( e, this ); // Logs the exception out to the Unity log.
				return float.NaN; // Returns float.NaN since this method has to return a value.
			}
		}

		/// <summary>
		/// Returns the average of the bands.
		/// </summary>
		/// <returns>Read the method header and description.</returns>
		public float GetAverage() {
			float average = 0f; // Set's the average to 0.

			foreach ( float flt in freqBand ) { // Loops through every band to get the value.
				average += flt; // Adds it to the average variable.
			}

			return average / freqBand.Length; // Returns average divided by the frequency band array length.
		}

		/// <summary>
		/// Calculates the average of the buffered bands.
		/// </summary>
		/// <returns>Read the method header and description.</returns>
		public float GetAverageBuffered() {
			float average = 0f; // Set's the average to 0.

			foreach ( float flt in bandBuffer ) { // Loops through all the buffered bands.
				average += flt; // Adds it to average.
			}

			return average / bandBuffer.Length; // Returns average divided by the frequency band array length.
		}

		private void GenerateBands() {
			int lastIndex = 0; // Stores the last index used to get a sample.

			for ( int i = 0; i < freqBand.Length; ++i ) { // Loops through the freqBand array.
				float average = 0f; // Used to calculate the average for that band.
				int sampleCount = GetSampleCount( i ); // Used to get the amount of samples to use.
				int tempLast = 0;

				for ( int j = lastIndex; j < sampleCount + lastIndex && j < samples.Length; ++j ) { // Loops through the samples until either it goes past sample count or samples.Length
					average += samples[j]; // Adds that sample to the average.
					tempLast = j + 1; // Set's lastIndex to a new value.
				}

				average /= sampleCount; // Averages out the average.

				freqBand[i] = average * 10; // Assigns that band to average times 10. Why 10? I don't fucking know, it was in V1 and it works, so don't change it.
				lastIndex = tempLast; // I don't remember what this was for, but removing it seems to make things worse.
			}
		}

		private int GetSampleCount( int position ) {
			float value = Mathf.Pow( 3f, position / ( samples.Length / 8 ) ); // Get's the asymptote value at that position.

			value = Mathf.Clamp( value, 1f, samples.Length ); // Used to ensure that it does not go over or under a range.

			return Mathf.RoundToInt( value ); // Rounds it to the nearest int.
		}

		private void GenerateBufferBand() {
			for ( int i = 0; i < bandBuffer.Length; ++i ) { // Used to ensure it goes through all the different bands.
				if ( freqBand[i] > bandBuffer[i] ) { // Checks to see if freq band at location i is bigger than it's corrosponing buffer.
					bandBuffer[i] = freqBand[i]; // Assigns the buffer at i to the freq band at i.
					bandDecrease[i] = 0.005f; // Reset's the decrease value.
				} else if ( freqBand[i] < bandBuffer[i] ) { // Checks to see if freq band at i is less than band buffer at i.
					bandBuffer[i] -= bandDecrease[i]; // Subtracts band decrease from band buffer.
					bandDecrease[i] *= decreaseSpeed; // Times the decrease by the decrease speed.
				}
			}
		}
	}
}
