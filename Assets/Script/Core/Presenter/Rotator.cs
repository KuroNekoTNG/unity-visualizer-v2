﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Visualizer.Core.Presenter {
	public class Rotator : MonoBehaviour {

		[Range(-50f, 50f), SerializeField]
		private float rotSpeed = 1f;

		// Update is called once per frame
		void Update() {
			transform.Rotate( Vector3.up * rotSpeed * Time.deltaTime );
		}
	}
}
