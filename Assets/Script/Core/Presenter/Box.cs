﻿using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

using Visualizer.Core.Audio;

namespace Visualizer.Core.Presenter {
	public class Box : MonoBehaviour {

		/// <summary>
		/// Used to set the location for the box to pull audio data from.
		/// </summary>
		public int BoxLoc {
			get;
			set;
		}

		/// <summary>
		/// Represents the color that the boxes are.
		/// </summary>
		public Color BoxColor {
			get;
			private set;
		}

		private Material material;
		private Bands bands;
		private PostProcessVolume ppVolume;
		private VisualizerManager vManager;

		// Use this for initialization
		private void Start() {
			PostProcessProfile ppProfile = ( PostProcessProfile )ScriptableObject.CreateInstance( typeof( PostProcessProfile ) );

			// These 4 lines grab their respective components.
			material = GetComponent<Renderer>().material;
			bands = GetComponentInParent<Bands>();
			vManager = GetComponentInParent<VisualizerManager>();
			ppVolume = GetComponent<PostProcessVolume>();

			ppProfile.settings = ppVolume.profile.settings;
			ppVolume.profile = ppProfile;
		}

		// Update is called once per frame
		private void Update() {
			float band;
			float newY;

			if ( vManager.UseBuffer ) { // Checks to see if it should use the buffer or not.
				band = bands.GetBandBuffer( BoxLoc ); // Grabs the buffer data.
			} else {
				band = bands.GetBand( BoxLoc ); // Grabs the unbuffered data.
			}

			newY = Mathf.Clamp( ( band * vManager.ScaleMultiplier ) + vManager.BoxSize, vManager.BoxSize, vManager.UpperBound ); // Clamps the size to be between the upper bound and the box size.

			transform.localScale = new Vector3( transform.localScale.x, newY, transform.localScale.z ); // Set's the localscale to a new vector3 that holds it's new size.

			if ( vManager.EnableSeasons ) { // This might be expanded later.
				if ( vManager.IsChristmas ) {
					BoxColor = vManager.ChristmasGradient.Evaluate( transform.localScale.y / vManager.UpperBound ); // Grabs the color for when it is christmas.
				} else if ( vManager.IsHalloween ) {
					BoxColor = vManager.HalloweenGradient.Evaluate( transform.localScale.y / vManager.UpperBound ); // Grabs the color for when it is halloween.
				} else if ( vManager.Is4thOfJuly ) {
					BoxColor = vManager.FourthOfJulyGradient.Evaluate( transform.localScale.y / vManager.UpperBound ); // Grabs the color for when it is the 4th of July.
				}
			} else {
				BoxColor = vManager.MainGradient.Evaluate( transform.localScale.y / vManager.UpperBound ); // Grabs the normal colors.
			}

			material.SetColor( "_Color", BoxColor ); // Set's the color of the box.
			material.SetColor( "_EmissionColor", BoxColor ); // Set's the box's emission color.
		}
	}
}
