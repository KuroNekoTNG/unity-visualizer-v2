﻿using System;
using System.Collections.Generic;

using UnityEngine;

using Visualizer.Core.Handlers;

namespace Visualizer.Core.Presenter {
	/// <summary>
	/// Handles the how many boxes and all of their properties as a single, global property.
	/// </summary>
	public class VisualizerManager : MonoBehaviour {
		private const string ENABLE_SEASON_KEY = "EnableSeasons", USE_BUFFER = "UseBuffer", BOX_SIZE = "BoxSize", UPPER_BOUND = "UpperBound", SCALE_MULTIPLIER = "ScaleMultiplier", NUMBER_OF_BOXES = "NumberOfBoxes", CUSTOM_GRADIENT = "CustomGradient", ENABLE_CUSTOM_GRADIENT = "EnableCustomGradient";

		#region Properties
		/// <summary>
		/// Changes the number of boxes.
		/// The number of boxes is restricted between 6 and 128 boxes.
		/// </summary>
		public int NumberOfBoxes {
			get {
				return numOfBoxes; // Returns the number of current boxes.
			}

			set {
				numOfBoxes = Mathf.Clamp( value, 8, 128 ); // Set's the number of boxes between 6 and 128.

				if ( !settings.TrySetValue( NUMBER_OF_BOXES, numOfBoxes ) ) {
					settings.Add( NUMBER_OF_BOXES, numOfBoxes );
				}
			}
		}

		/// <summary>
		/// Used to change how much the scale for their respective band.
		/// Will clamp the value between 1.15 and 512.
		/// </summary>
		public float ScaleMultiplier {
			get {
				return scaleMultiplier; // Returns the scale multiplier.
			}

			set {
				scaleMultiplier = Mathf.Clamp( value, 1.15f, 512f ); // Set's the scale multiplier to be between 1.15 and 512.

				if ( !settings.TrySetValue( SCALE_MULTIPLIER, scaleMultiplier ) ) {
					settings.Add( SCALE_MULTIPLIER, scaleMultiplier );
				}
			}
		}

		/// <summary>
		/// Used to get the maximum height for the boxes.
		/// Will clamp the set value between 1 and 15000.
		/// </summary>
		public float UpperBound {
			get {
				return upperBound; // Returns the maximum height.
			}

			set {
				upperBound = Mathf.Clamp( value, 1f, 15000f ); // Keeps the height between 1 and 15000.

				if ( !settings.TrySetValue( UPPER_BOUND, upperBound ) ) {
					settings.Add( UPPER_BOUND, upperBound );
				}
			}
		}

		/// <summary>
		/// Used to set how big the boxes will get.
		/// Will clamp the set value between 1 and 100.
		/// </summary>
		public float BoxSize {
			get {
				return boxSize; // Returns the scale size of the boxes.
			}

			set {
				boxSize = Mathf.Clamp( value, 1f, 100f ); // Set's the value of those boxes to be between 1 and 100

				if ( !settings.TrySetValue( BOX_SIZE, boxSize ) ) {
					settings.Add( BOX_SIZE, boxSize );
				}
			}
		}

		/// <summary>
		/// Used to tell the bands rather or not to use a buffer.
		/// </summary>
		public bool UseBuffer {
			get {
				return useBuffer; // Returns rather the boxes use the buffer or not.
			}

			set {
				useBuffer = value; // Used to set the buffer value.

				if ( !settings.TrySetValue( USE_BUFFER, value ) ) {
					settings.Add( USE_BUFFER, value );
				}
			}
		}

		/// <summary>
		/// A bool value deciding rather it is christmas season or not.
		/// </summary>
		public bool IsChristmas {
			get {
				DateTime now = DateTime.Now;

				return now.Month <= 12 && now.Month >= 11;
			}
		}

		/// <summary>
		/// A bool value indicating rather or not if it is halloween season.
		/// </summary>
		public bool IsHalloween {
			get {
				return DateTime.Now.Month == 10;
			}
		}

		/// <summary>
		/// A bool value indicating rather or not if it is the 4th of July.
		/// </summary>
		public bool Is4thOfJuly {
			get {
				DateTime now = DateTime.Now;

				return now.Month == 7 && now.Day == 4;
			}
		}

		/// <summary>
		/// Indicates rather the user wants season gradients or not.
		/// </summary>
		public bool EnableSeasons {
			get {
				return enableSeasons;
			}

			set {
				enableSeasons = value;

				if ( !settings.TrySetValue( ENABLE_SEASON_KEY, value ) ) {
					settings.Add( ENABLE_SEASON_KEY, value );
				}
			}
		}

		public Gradient MainGradient {
			get {
				return mainGradient;
			}
		}

		public Gradient ChristmasGradient {
			get {
				return christmasGradient;
			}
		}

		public Gradient HalloweenGradient {
			get {
				return halloweenGradient;
			}
		}

		public Gradient FourthOfJulyGradient {
			get {
				return fourthOfJulyGradient;
			}
		}
		#endregion

		#region Fields
		[Range( 0.01f, 500f ), SerializeField]
		private float radius = 1f; // Set's the size of the visualizer.
		private float oldRadius = 0f; // Used to see if the radius changed.
		[Range( 1f, 100f ), SerializeField]
		private float boxSize = 1f; // Set's the size of boxes.
		private float oldSizeOfBoxes = 0f;
		[Range( 1000f, 15000f ), SerializeField]
		private float upperBound = 100f;
		[Range( 0.1f, 1024f ), SerializeField]
		private float scaleMultiplier = 100f;
		[Range( 8, 126 ), SerializeField]
		private int numOfBoxes = 6; // Set's the number of boxes.
		private int oldNumOfBoxes = 0; // Used to see if the number changed.
		[SerializeField]
		private bool useBuffer, enableSeasons;

		[SerializeField]
		private GameObject spawnObject; // The thing to spawn.
		[SerializeField]
		private Gradient mainGradient, christmasGradient, halloweenGradient, fourthOfJulyGradient; // Used to decide what color the box should be.
		private SettingsManager settings;
		#endregion

		// Use this for initialization
		private void Start() {
			settings = SettingsManager.Singleton;

			if ( !Application.isEditor ) {
				LoadFromSettings();
			}
		}

		private void LoadFromSettings() {
			object tmp;

			if ( settings.TryGetValue( ENABLE_SEASON_KEY, out tmp ) ) {
				try {
					enableSeasons = ( bool )tmp;
				} catch ( InvalidCastException e ) {
					Debug.LogException( e, this );
					EnableSeasons = enableSeasons;
				}
			} else {
				EnableSeasons = enableSeasons;
			}

			if ( settings.TryGetValue( USE_BUFFER, out tmp ) ) {
				try {
					useBuffer = ( bool )tmp;
				} catch ( InvalidCastException e ) {
					Debug.LogException( e, this );
					UseBuffer = useBuffer;
				}
			} else {
				UseBuffer = useBuffer;
			}

			if ( settings.TryGetValue( BOX_SIZE, out tmp ) ) {
				try {
					boxSize = Convert.ToSingle( tmp );
				} catch ( InvalidCastException e ) {
					Debug.LogException( e, this );
					BoxSize = boxSize;
				}
			} else {
				BoxSize = boxSize;
			}

			if ( settings.TryGetValue( UPPER_BOUND, out tmp ) ) {
				try {
					upperBound = Convert.ToSingle( tmp );
				} catch ( InvalidCastException e ) {
					Debug.LogException( e, this );
					UpperBound = upperBound;
				}
			} else {
				UpperBound = upperBound;
			}

			if ( settings.TryGetValue( SCALE_MULTIPLIER, out tmp ) ) {
				try {
					scaleMultiplier = Convert.ToSingle( tmp );
				} catch ( InvalidCastException e ) {
					Debug.LogException( e, this );
					ScaleMultiplier = scaleMultiplier;
				}
			} else {
				ScaleMultiplier = scaleMultiplier;
			}

			if ( settings.TryGetValue( NUMBER_OF_BOXES, out tmp ) ) {
				try {
					numOfBoxes = Convert.ToInt32( tmp );
				} catch ( InvalidCastException e ) {
					Debug.LogException( e, this );
					NumberOfBoxes = numOfBoxes;
				}
			} else {
				NumberOfBoxes = numOfBoxes;
			}
		}

		// Update is called once per frame
		private void Update() {

			if ( ( Mathf.Abs( radius - oldRadius ) > 0.01f ) || oldNumOfBoxes != numOfBoxes ) { // Checks to see if either radius or number of boxes changed.
				RebuildVisualizer(); // Calls this to rebuild the visualizer.

				oldRadius = radius; // Reassings the old value to the new one.
				oldNumOfBoxes = numOfBoxes; // Reassings the old value to the new one.
			}

			if ( Mathf.Abs( oldSizeOfBoxes - boxSize ) > 0.0001f ) { // Checks to see if the size setting changed.
				foreach ( Transform child in transform ) { // Loops through all the child objects.
					child.localScale = new Vector3( boxSize, boxSize, boxSize ); // Changes their local size to that of the new one.
				}

				oldSizeOfBoxes = boxSize; // Set's the old value to the new one.
			}
		}

		// Used to rebuild the visualizer.
		private void RebuildVisualizer() {
			List<Vector3> newBoxPos = CalcBoxLocations(); // Get's a list of new positions for the boxes.

			foreach ( Transform child in transform ) { // Loops through all the child objects.
				Destroy( child.gameObject ); // Destroy's the child object.
			}

			BuildCirlceOfBoxes( newBoxPos ); // Builds the cirlce of boxes.
		}

		/// <summary>
		/// Builds the circle of boxes.
		/// </summary>
		/// <param name="boxPos">A list of new positions.</param>
		private void BuildCirlceOfBoxes( List<Vector3> boxPos ) { // Creates a new circle of boxes.
			for ( int i = 0; i < boxPos.Count; ++i ) { // Loops through the list of new box positions.
				GameObject child = Instantiate( spawnObject, boxPos[i], transform.rotation, transform ); // Creates a new child box.
				Box vBox = child.gameObject.GetComponent<Box>();

				child.transform.localScale = new Vector3( boxSize, boxSize, boxSize ); // Set's it's local scale to that of the size wanted.
				vBox.BoxLoc = i; // Set's the blocks location to i and also assigns it's band at the same time.
				vBox.gameObject.layer = gameObject.layer; // Set's the blocks layer to the current gameobjet's layer.
			}
		}

		/// <summary>
		/// Calculates where each box is going to be at.
		/// </summary>
		/// <returns>A list of new positions.</returns>
		private List<Vector3> CalcBoxLocations() {
			float degreePoints = 360f / numOfBoxes; // Get's the degree of which a new box should be at.
			List<Vector3> boxPos = new List<Vector3>(); // Creates a new list to store all of them.

#if UNITY_EDITOR
			Debug.Log( $"Degree's of seperation: {degreePoints}" ); // Logs how much of seperation each point has.
#endif

			for ( int i = 0; i < numOfBoxes; ++i ) { // Creates an integer used to increase the seperation value.
				float x, z, radians = ( i * degreePoints ) * Mathf.Deg2Rad; // Readies 2 empty variables and a third holds the radian of that box.

				x = transform.position.x + ( radius * Mathf.Cos( radians ) * Mathf.Sin( 90f * Mathf.Deg2Rad ) ); // Calculates the 3D place on the sphere for x.
				z = transform.position.z + radius * Mathf.Sin( radians ); // Calculates the 3D place on the sphere for z.

				// All boxes height will be the same as the parents.
				boxPos.Add( new Vector3( x, transform.position.y, z ) ); // Adds a new [Vector3] to the list of box positions.
			}

			return boxPos; // Returns the list of boxes.
		}

		/// <summary>
		/// Draws shit on the screen for debug purposes.
		/// Go look at the Unity Docs you fuck!
		/// </summary>
		private void OnDrawGizmosSelected() {
			float size;
			Gizmos.color = Color.red; // Set's the gizmo to green.

			Gizmos.DrawWireSphere( transform.position, radius ); // Draws a wire sphere to show how big the radius is.

			if ( 0 < transform.childCount ) {
				foreach ( Transform child in transform ) { // Loops through all the child objects.
					Gizmos.color = child.gameObject.GetComponent<Box>().BoxColor;

					Gizmos.DrawLine( transform.position, child.position ); // Creates a gizmo line from the root to the child.
				}
			} else {
				size = boxSize / 10;

				Gizmos.color = Color.green;

				foreach ( Vector3 boxPos in CalcBoxLocations() ) {
					Vector3 newBoxPos = new Vector3( boxPos.x, boxPos.y + size / 2, boxPos.z );

					Gizmos.DrawCube( newBoxPos, new Vector3( size, size, size ) );
				}
			}
		}
	}
}