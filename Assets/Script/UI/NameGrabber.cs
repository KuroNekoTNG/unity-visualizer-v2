﻿using TMPro;

using UnityEngine;

using Visualizer.Core.Handlers;

namespace Visualizer.UI {
	public class NameGrabber : MonoBehaviour {

		[SerializeField]
		private string key; // Get's the key for the text that is to be changed.

		private LanguageManager languageManager; // The language manager reference.
		private TextMeshProUGUI textComponent;

		// Use this for initialization
		private void Start() {
			languageManager = LanguageManager.Singleton; // Grabs the singleton reference.
			languageManager.Changed += Notify; // Adds the objects Notify method to the delegate.

			textComponent = GetComponent<TextMeshProUGUI>();

			Notify(); // Calls Notify to get the value.
		}

		private void Notify() {
			string value = languageManager[key]; // Grabs the text from the language manager.

			textComponent.text = value; // Assigns it to the text.
		}
	}
}