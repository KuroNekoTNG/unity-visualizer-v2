﻿using System;

using TMPro;

using UnityEngine;
using UnityEngine.UI;

using Visualizer.Core.Handlers;

namespace Visualizer.UI.Settings {
	public class UserInterfaceScalingHandler : MonoBehaviour {

		private const string UI_SCALE_SETTING_KEY = "UIScaleFactor"; // Holds the key for the UI Scale.

		[SerializeField]
		private TMP_InputField text; // Holds the text that will be used.
		[SerializeField]
		private Slider uiSlider; // Holds the slider.
		[SerializeField]
		private Canvas canvas; // Holds the reference to the canvas.
		private SettingsManager settingsManager; // Holds the reference to settings manager.

		// Use this for initialization
		private void Start() {
			settingsManager = SettingsManager.Singleton; // Grabs SettingsManager.

			if ( settingsManager.ConstainsKey( UI_SCALE_SETTING_KEY ) ) { // Checks to see if the key exists.
				try {
					uiSlider.value = Convert.ToSingle( settingsManager[UI_SCALE_SETTING_KEY] );
				} catch {
					Debug.LogError( "Failed to properly get float. Resetting the value.", this );
					settingsManager[UI_SCALE_SETTING_KEY] = uiSlider.value;
				}
			} else {
				settingsManager.Add( UI_SCALE_SETTING_KEY, uiSlider.value );
			}

			text.text = uiSlider.value.ToString( "N2" );
		}

		public void UIScaleChanged( string value ) {
			float fValue;

			if ( float.TryParse( value, out fValue ) ) {
				UIScaleChanged( fValue );
			} else {
				text.text = uiSlider.value.ToString( "N2" );
			}
		}

		public void UIScaleChanged( float value ) {
			value = Mathf.Clamp( value, 0.25f, 25 );

			text.text = value.ToString( "N2" );

			value = float.Parse( text.text );

			uiSlider.value = value;
		}

		public void ApplyValue() {
			canvas.scaleFactor = uiSlider.value;

			settingsManager[UI_SCALE_SETTING_KEY] = uiSlider.value;
		}
	}
}
