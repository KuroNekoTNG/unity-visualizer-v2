﻿using System;
using System.Collections.Generic;

using TMPro;

using UnityEngine;

using Visualizer.Core.Handlers;

namespace Visualizer.UI.Settings {
	/// <summary>
	/// Used manage all the settings.
	/// </summary>
	public class WindowSettingHandler : MonoBehaviour {
		private const string RESOLUTION_KEY_WIDTH = "Width", RESOLUTION_KEY_HEIGHT = "Height", REFRESH_KEY = "Refresh", WINDOW_TYPE = "WindowType";

		[SerializeField]
		private TMP_Dropdown resolutionDropdown, refreshDropdown, windowType; // Holds the reference to the dropdowns.
		private SettingsManager settings;

		// Use this for initialization
		private void Start() {
			settings = SettingsManager.Singleton; // Grabs a reference to the object that handles saving of the settings.

			LoadSettings();

			try {
				PopulateResAndRefreshDropdown(); // Executes the populate and refresh method.  
				PopulateWindowType(); // Populates the dropdown that holds the Window types.
			} catch ( Exception e ) {
				Debug.LogException( e, this );
			}
		}

		private void PopulateWindowType() {
			int i; // This is used to keep memory allocation/garbage collection small.
			List<string> windowTypes = new List<string>( Enum.GetNames( typeof( FullScreenMode ) ) ); // Get's the list of enum names for FullScreenMode enum.

			for ( i = 0; i < windowTypes.Count; ++i ) { // Loops through the list.
				string tmpString = windowTypes[i]; // Assigns a temp variable for the string.

				for ( int j = 0; j < tmpString.Length - 1; ++j ) { // Loops through the string.
					if ( !char.IsDigit( tmpString[j] ) && !char.IsWhiteSpace( tmpString[j] ) && // This line checks to see that the char at position j is not whitespace or a digist.
						!char.IsDigit( tmpString[j + 1] ) && !char.IsWhiteSpace( tmpString[j + 1] ) && // This line checks to see that the char at position j + 1 is not whitespace or a digit.
						char.IsLower( tmpString[j] ) && char.IsUpper( tmpString[j + 1] ) ) { // This line checks to see that the current char is lower and the next one is upper.

						tmpString = tmpString.Insert( ++j, " " ); // Inserts a space between the lowercase and uppercase chars. It also reassigns it to the tmpString variable.
					}
				}

				windowTypes[i] = tmpString; // Reassigns the string to the position it was retrived from.
			}

			windowType.AddOptions( windowTypes ); // Adds that list to the dropdown menu.

			windowType.value = ( int )Screen.fullScreenMode; // Assigns the fullscreen value to the value of the dropdown.
		}

		// Used to load the settings.
		private void LoadSettings() {
			object width, height, refresh, screenMode; // Holds the values that are retrived from the settings manager.

			if ( settings.TryGetValue( RESOLUTION_KEY_WIDTH, out width ) && settings.TryGetValue( RESOLUTION_KEY_HEIGHT, out height ) && settings.TryGetValue( REFRESH_KEY, out refresh ) && settings.TryGetValue( WINDOW_TYPE, out screenMode ) ) {
				// Sets the current screen resolution, refresh, and fullscreen mode.
				Screen.SetResolution( Convert.ToInt32( width ), Convert.ToInt32( height ), // Set's the width and height.
					( FullScreenMode )Enum.ToObject( typeof( FullScreenMode ), screenMode ), // Set's the fullscreen mode using this complexe looking line.
					Convert.ToInt32( refresh ) ); // Set's the refresh rate.
			}

			LoadGraphicSettings();
		}

		private void LoadGraphicSettings() {
			
		}

		// This method handles the populating of the resolution dropdown and the refresh rate dropdown.
		private void PopulateResAndRefreshDropdown() {
			List<string> resolutions = new List<string>(), refreshs = new List<string>(); // Creates a list of strings for refresh rates and resolutions.
			int sameResLoc = 0, sameRefreshLoc = 0;

			foreach ( Resolution res in Screen.resolutions ) { // Loops through all the Resolutions available.
				string resString = $"{res.width}x{res.height}"; // Creates a res string that holds the width and height.

				if ( Debug.isDebugBuild || Application.isEditor ) { // Checks to see if it is in a debug build or the editor.
					Debug.Log( $"Found resolution: {resString}", this ); // Used show that it found a resolution. 
				}

				if ( !resolutions.Contains( resString ) ) { // Checks if the resolution is not in the resolutions list.
					if ( Debug.isDebugBuild || Application.isEditor ) {
						Debug.Log( $"Resolution added: {resString}", this );  // Used to indicate in the editor that this script has added a new resolution. 
					}

					resolutions.Add( resString ); // Adds the resolution to the list.

					if ( resString == $"{Screen.width}x{Screen.height}" ) { // Checks to see if that res string matches the current resolution.
						sameResLoc = resolutions.Count - 1; // Notes its location in the list.
					}
				}

				if ( !refreshs.Contains( res.refreshRate.ToString() ) ) { // Checks to see if the refresh rate is not already in the list.
					refreshs.Add( res.refreshRate.ToString() ); // Adds the refresh rate to the list.

					if ( res.refreshRate == Screen.currentResolution.refreshRate ) { // Checks to see if it matches the current refresh rate.
						sameRefreshLoc = refreshs.Count - 1; // Notes its location in the list.
					}
				}
			}

			resolutionDropdown.AddOptions( resolutions ); // Passes the resolutions list to the resolution dropdown.
			refreshDropdown.AddOptions( refreshs ); // Passes the refreshes list to the resolution dropdown.

			resolutionDropdown.value = sameResLoc; // Sets its value to the noted position.
			refreshDropdown.value = sameRefreshLoc; // Sets its value to the noted position.
		}

		/// <summary>
		/// Used to note that a window option has changed.
		/// </summary>
		public void OnWindowSettingsChanged() {
			string[] widthHeight = resolutionDropdown.options[resolutionDropdown.value].text.Split( 'x' ); // Grabs the width and height from the resolution dropdown.
			int width, height, refresh; // Used later.

			if ( int.TryParse( widthHeight[0], out width ) && int.TryParse( widthHeight[1], out height )
				&& int.TryParse( refreshDropdown.options[refreshDropdown.value].text, out refresh ) ) { // Used to parse the strings to ints, if it failed, it does not change anything.
				Screen.SetResolution( width, height, ( FullScreenMode )windowType.value ); // This will do nothing in the Editor, so it is fine.

				if ( Debug.isDebugBuild || Application.isEditor ) {
					Debug.Log( $"Resolution would of changed to: {width}x{height} @ {refresh}FPS @ {Screen.fullScreenMode.ToString()}" );
				}

				if ( !settings.TrySetValue( RESOLUTION_KEY_WIDTH, width ) ) { // Checks to see if it can set those values in settings.
					settings.Add( RESOLUTION_KEY_WIDTH, width ); // Adds those values if they do not exist.
				}

				if ( !settings.TrySetValue( RESOLUTION_KEY_HEIGHT, height ) ) { // Checks to see if it can set those values in settings.
					settings.Add( RESOLUTION_KEY_HEIGHT, height ); // Adds those values if they do not exist.
				}

				if ( !settings.TrySetValue( REFRESH_KEY, refresh ) ) { // Checks to see if it can set those values in settings.
					settings.Add( REFRESH_KEY, refresh ); // Adds those values if they do not exist.
				}

				if ( !settings.TrySetValue( WINDOW_TYPE, ( FullScreenMode )windowType.value ) ) { // Tries to change the window type.
					settings.Add( WINDOW_TYPE, ( FullScreenMode )windowType.value ); // Adds the key and value.
				}
			} else {
				// TODO: Add popup window.
				Debug.Log( $"Unable to change the resolution and/or refresh rate:\n{widthHeight[0]}x{widthHeight[1]}x{refreshDropdown.options[refreshDropdown.value]}",
					this ); // Logs that it was unable to log the new values.
			}
		}
	}
}
