﻿using System;

using UnityEngine;
using UnityEngine.Rendering.PostProcessing;
using UnityEngine.UI;

using Visualizer.Core.Handlers;

namespace Visualizer.UI.Settings {
	public class PostProcessingSettingsHandler : MonoBehaviour {

		private const string POST_PROC_TOGGLE = "PostProc";

		[SerializeField]
		private Toggle postProcToggle;
		[SerializeField]
		private PostProcessLayer mainCamera;
		private SettingsManager settings;

		// Use this for initialization
		void Start() {
			settings = SettingsManager.Singleton; // Grabs the SettingsManager singleton reference.

			if ( settings.ConstainsKey( POST_PROC_TOGGLE ) ) { // Checks to see if the PostProc value exists.
				postProcToggle.isOn = Convert.ToBoolean( settings[POST_PROC_TOGGLE] ); // Grabs it and converts it to a boolean.
			} else {
				settings.Add( POST_PROC_TOGGLE, postProcToggle.isOn ); // Adds the value otherwise.
			}

			PostProcessingToggle(); // Calls to make sure it is set for the camera.
		}

		/// <summary>
		/// Used to enable or disable post processing.
		/// </summary>
		public void PostProcessingToggle() {
			mainCamera.enabled = postProcToggle.isOn; // Set's the behaviour to what the value is.

			settings.SetValue( POST_PROC_TOGGLE, postProcToggle.isOn ); // Set's the value to the what the toggle is.
		}
	}
}
