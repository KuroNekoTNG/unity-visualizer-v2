﻿using System;

using TMPro;

using UnityEngine;

using Visualizer.Core.Handlers;

namespace Visualizer.UI.Settings {
	public class VSyncSettingsHandler : MonoBehaviour {

		private const string VSYNC_SETTING = "VSyncBufferSize";

		[SerializeField]
		private TMP_Dropdown vsyncDropdown;
		private SettingsManager settings;

		// Use this for initialization
		void Start() {
			settings = SettingsManager.Singleton;

			if ( settings.ConstainsKey( VSYNC_SETTING ) ) {
				vsyncDropdown.value = Convert.ToInt32( settings.GetValue( VSYNC_SETTING ) ); // Grabs the value from the settings and turns it into an int.
			} else {
				settings.Add( VSYNC_SETTING, vsyncDropdown.value ); // Adds the value to the settings.
			}

			vsyncDropdown.value = QualitySettings.vSyncCount;
		}

		/// <summary>
		/// Updates the VSync option.
		/// </summary>
		public void VSyncChanged() => QualitySettings.vSyncCount = vsyncDropdown.value; // Uses the value to change the VSync option.
	}
}
