﻿using System;

using TMPro;

using UnityEngine;

using Visualizer.Core.Handlers;

namespace Visualizer.UI.Settings {
	public class AntiAliasingSettingsHandler : MonoBehaviour {

		private const string ANTI_ALIASING_KEY = "AntiAliasing";

		[SerializeField]
		private TMP_Dropdown aaValues;
		private SettingsManager settings;

		// Use this for initialization
		void Start() {
			string option;
			int level;

			settings = SettingsManager.Singleton;

			if ( settings.ConstainsKey( ANTI_ALIASING_KEY ) ) {
				option = Convert.ToString( settings[ANTI_ALIASING_KEY] );

				if ( option.Equals( "Off", StringComparison.OrdinalIgnoreCase ) ) {
					QualitySettings.antiAliasing = 0;
				} else if ( int.TryParse( option, out level ) ) {
					QualitySettings.antiAliasing = level;
				} else {
					QualitySettings.antiAliasing = 0;
					settings[ANTI_ALIASING_KEY] = "Off";
				}

				SetDropdownToValue();
			} else {
				settings.Add( ANTI_ALIASING_KEY, "Off" );
			}
		}

		private void SetDropdownToValue() {
			if ( QualitySettings.antiAliasing == 0 ) {
				aaValues.value = 0;
			} else {
				for ( int i = 0; i < aaValues.options.Count; ++i ) {
					if ( aaValues.options[i].text == QualitySettings.antiAliasing.ToString() ) {
						aaValues.value = i;
						return;
					}
				}
			}
		}

		public void AAChanged( int value ) {
			QualitySettings.antiAliasing = aaValues.options[value].text == "Off" ? 0 : int.Parse( aaValues.options[value].text );

			settings[ANTI_ALIASING_KEY] = QualitySettings.antiAliasing;
		}
	}
}
