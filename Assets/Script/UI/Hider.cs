﻿using UnityEngine;

namespace Visualizer.UI {
	public class Hider : MonoBehaviour {

		[Range( 10f, 1000f ), SerializeField]
		private float idleTime = 50f; // This contols how long the user has to be idle in seconds before it activates.
		private float timeSinceIdle; // Used to keep track of how long.
		private bool hidden = false; // Used as a flag to see which method to call.
		[SerializeField]
		private bool useIdleTime = true; // Used to know if it should use the idle time or not. NOTE: This is not for debug purposes only, this can be used for something else.

		private Vector3 oldMousePos; // Keeps track of the old mouse position.
		private Animator animator; // Holds the animator in case of future use.

		// Use this for initialization
		private void Start() {
			if ( animator == null ) { // Checks to see if one has already been asigned.
				animator = GetComponent<Animator>(); // Get's the animator.
			}

			hidden = animator.GetCurrentAnimatorStateInfo( 0 ).IsName( "Move Out" ); // Checks to see if the animator is in the hidden mode.
		}

		// Update is called once per frame
		private void Update() {
			if ( useIdleTime ) { // Checks if useIdleTime is true.
				CheckIdleStatus(); // Calls this method to see how long the user has been idle.
			} else if ( hidden ) {
				//if(UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject())
			}
		}

		private void CheckIdleStatus() {
			if ( Input.mousePosition == oldMousePos ) { // CHecks to see if mouse has not moved yet.
				if ( !hidden ) { // Checks if hidden is false.
					if ( idleTime <= timeSinceIdle ) { // Checks if the idle time mark is less than the amount the user as been idle.
						HideWindow(); // Calls the hide window method.
					}

					timeSinceIdle += Time.deltaTime; // Add's the time difference to the idle time.
				}
			} else {
				oldMousePos = Input.mousePosition; // Set's oldMousePos to the current mouse position.

				timeSinceIdle = 0f; // Set's idle time to 0.

				if ( hidden ) { // Checks to see if hidden is enabled.
					ShowWindow(); // Calls the show window method.
				}
			}
		}

		/// <summary>
		/// Used to tell the UI element to hide itself.
		/// </summary>
		public void HideWindow() {
			hidden = true; // Set's hidden to true.

			animator.SetTrigger( "user_idle" ); // Tells the animator to trigger the object's move out of animation.
		}

		/// <summary>
		/// Used to tell the UI element to show itself.
		/// </summary>
		public void ShowWindow() {
			hidden = false; // Set's hidden to false.

			animator.SetTrigger( "user_moved" ); // Tells the animator to trigger the object's move into animation.
		}

		/// <summary>
		/// Toggles between the two animations.
		/// </summary>
		public void ToggleShowing() {
			if ( hidden ) { // Checks if the hidden bool is true.
				ShowWindow(); // Calls ShowWindow.
			} else {
				HideWindow(); // Otherwise calls HideWindow.
			}
		}
	}
}
