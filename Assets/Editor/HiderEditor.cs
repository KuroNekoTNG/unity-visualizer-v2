﻿using UnityEditor;

using Visualizer.UI;

/// <summary>
/// Used to show what the inspector for Unity sees, and to clean things up a bit.
/// </summary>
[CustomEditor( typeof( Hider ) )]
public class HiderInspectorController : Editor {
	private SerializedProperty idleTime, useIdleTime; // Holds the values for the serialized objects.

	// Initializes the controller. See the Unity3D docs about this guy.
	private void Awake() {
		idleTime = serializedObject.FindProperty( "idleTime" ); // Grabs the idleTime property.
		useIdleTime = serializedObject.FindProperty( "useIdleTime" ); // Grabs the useIdleTime boolean.
	}

	/// <summary>
	/// Used to change what the Inspector sees.
	/// See: <see cref="Editor.OnInspectorGUI"/>
	/// </summary>
	public override void OnInspectorGUI() {
		serializedObject.Update(); // Get's the most recent version of the object.

		EditorGUILayout.PropertyField( useIdleTime ); // Displays the boolean on the inspector.

		if ( useIdleTime.boolValue ) { // Checks to see if useIdleTime is true.
			EditorGUILayout.PropertyField( idleTime ); // If useIdleTime is true, display the float value.
		}

		serializedObject.ApplyModifiedProperties(); // Applies the modified properties.
	}
}