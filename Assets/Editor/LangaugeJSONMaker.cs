﻿using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;

using UnityEditor;

using UnityEngine;

public class LangaugeJSONMaker : EditorWindow {

	private string languagePackName; // Used to store the name of the language pack.
	private int index; // Used to store the index the user wishes to delete.

	private List<StringPair> pairs = new List<StringPair>(); // Used to store a pair of string for use in a language pack.
	private Vector2 scrollPos; // Used to tell Unity where to place the scroll view.

	private string LanguagePackLoc;

	// Used to tell Json.NET how to write to a json file.
	private readonly JsonSerializerSettings serializerSettings = new JsonSerializerSettings() {
		DateFormatHandling = DateFormatHandling.IsoDateFormat,
		ConstructorHandling = ConstructorHandling.AllowNonPublicDefaultConstructor,
		DateParseHandling = DateParseHandling.DateTime,
		DateTimeZoneHandling = DateTimeZoneHandling.Utc,
		FloatFormatHandling = FloatFormatHandling.String,
		DefaultValueHandling = DefaultValueHandling.IgnoreAndPopulate,
		FloatParseHandling = FloatParseHandling.Double,
		Formatting = Formatting.Indented,
		MetadataPropertyHandling = MetadataPropertyHandling.Default,
		MissingMemberHandling = MissingMemberHandling.Ignore,
		NullValueHandling = NullValueHandling.Include,
		ObjectCreationHandling = ObjectCreationHandling.Auto,
		ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
		PreserveReferencesHandling = PreserveReferencesHandling.None,
		StringEscapeHandling = StringEscapeHandling.Default,
		TypeNameHandling = TypeNameHandling.None,
		TypeNameAssemblyFormatHandling = TypeNameAssemblyFormatHandling.Full
	};

	/// <summary>
	/// Used to tell Unity where to place the Window at.
	/// </summary>
	[MenuItem( "Window/JSON Language Pack Maker" )]
	public static void ShowWindow() {
		GetWindow<LangaugeJSONMaker>( "JSON Language Pack Maker" ); // Get's the window of this object.
	}

	private void OnEnable() {
		LanguagePackLoc = Path.Combine( Application.streamingAssetsPath, "Language" );
	}

	// See Unity's doc for this.
	private void OnGUI() {
		languagePackName = EditorGUILayout.TextField( "Language:", languagePackName ); // A text field for what the language pack name should be.

		CreatePairList(); // See method comment.
		CreateRemoveArea(); // See method comment.

		if ( GUILayout.Button( "Add new pair" ) ) {
			pairs.Add( new StringPair() );
		}

		GUILayout.EndHorizontal();

		GUILayout.BeginHorizontal();

		if ( !string.IsNullOrWhiteSpace( languagePackName ) && GUILayout.Button( $"Save to {languagePackName}.json" ) ) {
			SaveToJson();
		}

		AskIfFound();

		GUILayout.EndHorizontal();
	}

	private void AskIfFound() {
		Dictionary<string, string> temp;

		foreach ( FileInfo file in new DirectoryInfo( LanguagePackLoc ).EnumerateFiles( "*.json", SearchOption.AllDirectories ) ) { // Loops through every .json it finds in every sub-directory of the language pack folder.
			bool isEqual = file.Name.Equals( $"{languagePackName}.json", StringComparison.InvariantCultureIgnoreCase ); // Get's the boolean for it the names match.

			if ( isEqual && GUILayout.Button( $"Load {languagePackName}.json" ) ) { // Checks if isEqual is true and the user pressed the button.
				temp = JsonConvert.DeserializeObject<Dictionary<string, string>>( File.ReadAllText( file.FullName ), serializerSettings ); // Deserializes the language pack.

				foreach ( KeyValuePair<string, string> pair in temp ) {
					pairs.Add( new StringPair() {
						key = pair.Key,
						value = pair.Value
					} );
				}
			}
		}
	}

	// Creates the remove area that the user will use to remove pairs.
	private void CreateRemoveArea() {
		GUILayout.Label( "Remove Pair Section", EditorStyles.largeLabel ); // Labels the area.

		index = EditorGUILayout.IntField( "Remove Index", index ); // Prints out a number that the user can change and takes in that change.

		GUILayout.BeginHorizontal();

		if ( GUILayout.Button( "Remove pair" ) ) { // Prints a button and checks to see if it returns true.
			if ( pairs.Count <= 0 ) { // Checks to see if pairs is not empty.
				Debug.LogError( "The pair list is empty.", this ); // Output's to the log that the user attempted to remove something from an empty list.
			} else if ( index < 0 ) { // Checks if the index the user inputed is negitive.
				Debug.LogError( "The index value is negitive.", this ); // Output's to the log that the user attempted to remove a pair using a negitive number.
			} else if ( index >= pairs.Count ) { // Checks if the index the user inputed is bigger than or equal to the amount of pairs.
				Debug.LogError( "The index value does not match a valid index.", this ); // Output;s to the log that the user attempted to remove a pair using a number that was out of bounds.
			} else {
				pairs.RemoveAt( index ); // Removes the pair at said index.
			}
		}
	}

	// Creates the list of pairs seen in the Unity Editor.
	private void CreatePairList() {
		scrollPos = EditorGUILayout.BeginScrollView( scrollPos ); // Starts a scroll view.

		for ( int i = 0; i < pairs.Count; ++i ) { // Loops through all elements of the pairs list.
			StringPair pair = pairs[i]; // Stores the pair in a local variable.

			GUILayout.Label( $"Pair {i + 1}:", EditorStyles.boldLabel ); // Labels the pair.

			pair.key = EditorGUILayout.TextField( "Key:", pair.key ); // Prints the text field and assigns whatever the user types in to the pair's key.
			pair.value = EditorGUILayout.TextField( "Value:", pair.value ); // Prints the text field and assigns whatever the user types in to the pair's value.

			pairs[i] = pair; // Reassigns the pair.
		}

		EditorGUILayout.EndScrollView(); // Ends the scroll view.
	}

	private void SaveToJson() {
		string fileContent;
		int numOfDuplicates = 0;
		Dictionary<string, string> output = new Dictionary<string, string>();

		foreach ( StringPair pair in pairs ) { // Loops through all the pairs.
			if ( !output.ContainsKey( pair.key ) ) { // Checks to see if skip is false.
				output.Add( pair.key, pair.value ); // Adds a new key value pair to the list.
			} else {
				++numOfDuplicates; // Incriments the number of dumplicates by 1.
			}
		}

		fileContent = JsonConvert.SerializeObject( output, serializerSettings ); // Uses Newtonsoft.Json to convert it to a json file.

		File.WriteAllText( Path.Combine( LanguagePackLoc, $"{languagePackName}.json" ), fileContent ); // Writes the string Newtonsoft.Json creates to a file.

		if ( numOfDuplicates > 0 ) { // Checks to see if the number of duplicates is greater than 0.
			Debug.Log( $"Number of duplicate keys are: {numOfDuplicates}", this ); // Output the number of duplicates.
		}
	}
}

internal struct StringPair {
	public string key, value;
}
