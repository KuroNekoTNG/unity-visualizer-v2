﻿using UnityEditor;

using UnityEngine;

using Visualizer.Core.Presenter;

[CustomEditor( typeof( VisualizerManager ) ), CanEditMultipleObjects]
public class VisualizerManagerEditor : Editor {

	private SerializedProperty radius, boxSize, upperBound, scaleMultiplier, numberOfBoxes, useBuffer, enableSeasons, spawnObject, mainGradient, christmasGradient, halloweenGradient, fourthOfJulyGradient;
	private GUIStyle style;

	private void Awake() {
		radius = serializedObject.FindProperty( "radius" ); // Grabs the radius propery.
		boxSize = serializedObject.FindProperty( "boxSize" ); // Grabs the boxSize property.
		upperBound = serializedObject.FindProperty( "upperBound" ); // Grabs the upperBound property.
		scaleMultiplier = serializedObject.FindProperty( "scaleMultiplier" ); // Grabs the scaleMultiplier property.
		numberOfBoxes = serializedObject.FindProperty( "numOfBoxes" ); // Grabs the numberOfBoxes property.
		useBuffer = serializedObject.FindProperty( "useBuffer" ); // Grabs the useBuffer property.
		enableSeasons = serializedObject.FindProperty( "enableSeasons" ); // Grabs the enableSeasons property.
		spawnObject = serializedObject.FindProperty( "spawnObject" ); // Grabs the spawnObject property.
		mainGradient = serializedObject.FindProperty( "mainGradient" ); // Grabs the spawnObject property.
		christmasGradient = serializedObject.FindProperty( "christmasGradient" ); // Grabs the spawnObject property.
		halloweenGradient = serializedObject.FindProperty( "halloweenGradient" ); // Grabs the spawnObject property.
		fourthOfJulyGradient = serializedObject.FindProperty( "fourthOfJulyGradient" ); // Grabs the spawnObject property.

		style = new GUIStyle { // Creates a new GUIStyle.
			alignment = TextAnchor.MiddleCenter,
			fontStyle = FontStyle.Bold,
			wordWrap = true
		};
	}

	public override void OnInspectorGUI() {
		serializedObject.Update(); // Updates the serializedObjects data.

		// Ranges
		EditorGUILayout.PropertyField( radius ); // Displays the radius property.

		// Number of box range.
		EditorGUILayout.PropertyField( numberOfBoxes ); // Displays the numberOfBoxes property

		// Adds a label for to show that it will modify box values.
		EditorGUILayout.LabelField( "Global Box Values", style ); // Creates a new label.

		// Box properties ranges.
		EditorGUILayout.PropertyField( boxSize ); // Displays the sizeOfBoxes property.
		EditorGUILayout.PropertyField( upperBound ); // Displays the upperBound property.
		EditorGUILayout.PropertyField( scaleMultiplier ); // Displays the scaleMultiplier property.

		// Use buffer option.
		EditorGUILayout.PropertyField( useBuffer ); // Displays the useBuffer property.

		// Enable seasons option.
		EditorGUILayout.PropertyField( enableSeasons );

		// Boxes to spawn.
		EditorGUILayout.PropertyField( spawnObject ); // Displays the spawnObject property.

		EditorGUILayout.LabelField( "Gradients", style );

		EditorGUILayout.PropertyField( mainGradient );
		EditorGUILayout.PropertyField( christmasGradient );
		EditorGUILayout.PropertyField( halloweenGradient );
		EditorGUILayout.PropertyField( fourthOfJulyGradient );

		serializedObject.ApplyModifiedProperties(); // Applies the modified properties.
	}
}
